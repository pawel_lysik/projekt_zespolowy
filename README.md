# README #

Project contains script to create customised iso image with archiso tool.  

Extensions to standard iso image:  
- custom packages installed: Chromium, NetworkManager, gnome environment  
- custom user   
- desktop environment installed  
- minimalistic custom service: hello  
- autostart of desktop environment at system setup  
- working both with UEFI and BIOS    

### Installation
The only prerequisite apart from this repository is `archiso` package.  

Installation command:   
`$ sudo pacman -S archiso`

### Creating image with script
Execute this command with sudo priviledges:   
`$ ./create_customized_image.sh <src repo> <dest repo> <build dir> <work dir>`  
Argument description:  
`src repo` - It's copy will be used as a starting point to customizing image e.g. /usr/share/archiso/configs/baseline/  
`dest repo` - Directory where profile will be copied  
`build dir` - Will contain .iso image  
`work dir` - Directory that will be used by archiso during image creation  

Script execution takes 10 - 15 minutes.   


### Running archiso   
Execute this command to test iso using QEMU (qemu and edk2-ovmf packages need to be installed):   
`$ run_archiso -i /path/to/archlinux-yyyy.mm.dd-x86_64.iso`


### Testing functionalities 

Custom service:  
Execute following command as root:  
`systemctl start hello`

On start service should create empty `/root/hello_started.txt` file.
After starting the service you can check if it's active by running `systemctl status hello`  

Custom user:  
It should be possible to login as user: archie with password: archie  

### BIOS and UEFI  
Image created from releng profile can be run both using UEFI and BIOS    

Run image using BIOS:  
```
$ run_archiso -i /path/to/archlinux-yyyy.mm.dd-x86_64.iso
```

Run image with UEFI emulation:  
```
$ run_archiso -u -i /path/to/archlinux-yyyy.mm.dd-x86_64.iso
```
