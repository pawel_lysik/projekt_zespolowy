

if [ $# -ne 4 ]
then
	echo "Usage <source repo e.g. /usr/share/archiso/configs/releng/> <dest to copy src repo> <build directory> <work directory>"
	exit 0
fi

source_profile=$1	# It's copy will be used as a starting point to customizing image
dest_profile=$2		# Directory where profile will be copied
build_dir=$3		# Will contain .iso image
work_dir=$4		# Directory that will be used by arch_iso during image creation


##############################################
###########    CLEANUP      #################
#############################################

echo "Remove ${dest_profile}, ${work_dir}, ${build_dir}"
[ -e ${dest_profile} ] && rm -rf ${dest_profile}
[ -e ${work_dir} ] && rm -rf ${work_dir}
[ -e ${build_dir} ] && rm -rf ${build_dir}

echo "Copy ${source_profile} to ${dest_profile}"
cp -r ${source_profile} ${dest_profile}


###############################################
#############     ADD USER 	###############
###############################################

user=archie
user_password_file=password.txt

echo Add  ${user} to ${dest_parofile} with UID and GID 1000

passwd_path="${dest_profile}/airootfs/etc/passwd"
shadow_path="${dest_profile}/airootfs/etc/shadow"
gshadow_path="${dest_profile}/airootfs/etc/gshadow"
group_path="${dest_profile}/airootfs/etc/group"

echo "root:x:0:0:root:/root:/bin/sh"
echo "${user}:x:1000:1000::/home/${user}:/bin/sh" >> ${passwd_path}

openssl_pwd=$(openssl passwd -6 -in ${user_password_file})
echo "${user}:${openssl_pwd}:14871::::::" >> ${shadow_path}

echo "root:!!::root" >> ${gshadow_path}
echo "${user}:!!::" >> ${gshadow_path}

echo "root:x:0:root" >> ${group_path}
echo "${user}:x:1000:" >> ${group_path}

######################################################
############	ADD CUSTOM PACKETS	##############
######################################################

echo "Add custom packets: X, KDE, Chromium, NetworkManager"
additional_packets_file="packets.txt"
basic_packets_file="${dest_profile}/packages.x86_64"

cat ${additional_packets_file} >> ${basic_packets_file}

##################################################
#####   ADD MINIMALISTIC CUSTOM SERVICE   ########
##################################################

mkdir -p "${dest_profile}/airootfs/etc/systemd/system/"

cp hello.service "${dest_profile}/airootfs/etc/systemd/system/"

current_dir=$(pwd)
mkdir -p "${dest_profile}/airootfs/etc/systemd/system/multi-user.target.wants"
cd "${dest_profile}/airootfs/etc/systemd/system/multi-user.target.wants"
ln -s /etc/systemd/system/hello.service
cd "$current_dir"

##################################################
#####           ENABLE AUTOLOGIN          ########
##################################################
mkdir -p "${dest_profile}/airootfs/etc/systemd/system/getty@tty1.service.d/"
cp autologin.conf "${dest_profile}/airootfs/etc/systemd/system/getty@tty1.service.d/"

####################################################
#############   BUILD IMAGE   ######################
###################################################

echo "Build ISO.  This may take some time...."
mkarchiso -v -w ${work_dir} -o ${build_dir} ${dest_profile}


